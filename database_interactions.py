import psycopg2
from configparser import ConfigParser
import pandas as pd

def load_config(filename='database.ini', section='postgresql'):
    parser = ConfigParser()
    parser.read(filename)

    # get section, default to postgresql
    config = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            config[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return config

def query_db(query, config):
    with psycopg2.connect(**config) as connection:
        with connection.cursor() as cursor:
            cursor.execute(query) # run the query
            rows = cursor.fetchall() # retrieve the results data
            column_names = [x.name for x in cursor.description] # get the column names of results
            df = pd.DataFrame(rows,columns=column_names) # put in a data frame

    return df